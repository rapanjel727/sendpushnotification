<?php

    // This firebase server key
    define('FCM_PUSH_KEY', 'AAAAdQGTeTE:APA91bF4hdBEjJr6jSdf7YjF5p0e-yPh6FgQy4eoAv6ELmzuFOeNBFDEZ92MFOZMP6T_nbjD16SnRHflu7Bbp_7QzadeeTljOWZEJ5SeHBflaPlmDtgsgS0Jyi-ibuZ-OiNb6VpUHCaa');

    $token     = $_POST['token'];
    $badge     = 2;                 // user can customize this
    $title      = 'Test_title';     // user can customize this
    $message    = 'Test message';   // user can customize this

    $url = "https://fcm.googleapis.com/fcm/send";
    
    if ($token == "" ) {
        return;
    }       

    $msg = array (
            'body'      => $message,
            'title'     => $title,   
            'badge'     => $badge,             
            'sound'     => 'default',
        ); 
    
    $data = array(
            'msgType' => "msgType",
            'content' => "contentcontentcontentcontent"
        );         

    $fields = array(
            'to'                => $token,
            'notification'      => $msg,
            'priority'          => 'high',
            'data'              => $data
        );
    
    $headers = array(
        'Authorization: key=' . FCM_PUSH_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);  
    
    $result = curl_exec($ch); 

    if (curl_errno($ch)) {
        echo curl_error($ch);
    } else {
        echo $result;
    }

    curl_close($ch);

    header('Content-Type: application/json');
    // echo json_encode($result);