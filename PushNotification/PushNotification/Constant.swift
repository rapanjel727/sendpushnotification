//
//  Constant.swift
//  PushNotification
//
//  Created by AngelDev on 9/9/20.
//  Copyright © 2020 AngelDev. All rights reserved.
//



class CONSTANT {
    
    
    // Request Params
    // Response parameters
    static let RES_MESSAGE                  = "message"
    static let DATA                         = "data"
    static let USER_ID                      = "ID"

    // Result code
    static let RESULT_CODE                  = "result_code"
    static let CODE_SUCCESS                 = 200
    static let CODE_FAIL                    = 400
    static let CODE_201                     = 201
    static let CODE_202                     = 202
    static let CODE_203                     = 203
    static let CODE_404                     = 404
}
