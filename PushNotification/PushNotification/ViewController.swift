//
//  ViewController.swift
//  PushNotification
//
//  Created by AngelDev on 9/9/20.
//  Copyright © 2020 AngelDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblMyFCMToken: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showBadge), name: Notification.Name("GET_FMC_Token"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lblMyFCMToken.text = deviceTokenString
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    @objc func showBadge() {
        lblMyFCMToken.text = deviceTokenString
    }
    
    @IBAction func didTapSendNoticiation(_ sender: Any) {

        
        APIManager.shared.sendPush(deviceTokenString) { (isSuccess, msg) in
            var title = "Result"
            var message = ""
            if isSuccess == CONSTANT.CODE_SUCCESS {
                message = "Success"
            } else {
                title = "Failed"
                message = msg
            }
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            DispatchQueue.main.async(execute:  {
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    @IBAction func didTapWrite(_ sender: Any) {
        APIManager.shared.writeToken(deviceTokenString) { (isSuccess, msg) in
            var title = "Result"
            var message = ""
            if isSuccess == CONSTANT.CODE_SUCCESS {
                message = "Success"
            } else {
                title = "Failed"
                message = msg
            }
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            DispatchQueue.main.async(execute:  {
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    
}

