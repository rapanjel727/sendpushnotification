//
//  APIManager.swift
//  PushNotification
//
//  Created by AngelDev on 9/9/20.
//  Copyright © 2020 AngelDev. All rights reserved.
//

import Alamofire
import SwiftyJSON

//let HOST = "http://45.82.73.110/"
//let API = HOST + "iteller/"


let HOST = "http://192.168.101.180/"
let API = HOST + "TestAPI/"


class APIManager {
    
    let GET_SEND_PUSH              = API + "sendPush.php"
    let WRITE_TOKEN                = API + "writeToken.php"
    
    static let shared = APIManager()
    
    // MARK: - APIs
    ///
    /// - parameter completion:         Function to call when the response is obtained (Status: Bool, Message:  String?, Response: [String; Any]?
    /// - parameter token: device token to send
    func sendPush(_ token: String, completion: @escaping (Int, String) -> Void) {
        
        let params = ["token"    : token]
        AF.request(GET_SEND_PUSH, method: .post, parameters: params)
            .validate()
            .responseJSON { response in
                guard response.error == nil else {
                    completion(CONSTANT.CODE_FAIL, (response.error)!.localizedDescription)
                    return
                }

                // check response valud is valid
                guard let value = response.value as? [String: Any] else {
                    completion(CONSTANT.CODE_FAIL, "Internal server error is occured.")
                   return
                }
               
                let isSuccess = value["success"] as? Int
                
                if  isSuccess == 1 {
                    completion(CONSTANT.CODE_SUCCESS, "")
                } else {
                    completion(CONSTANT.CODE_FAIL, "Something went wrong.")
                }
            }
    }
    
    func writeToken(_ token: String, completion: @escaping (Int, String) -> Void) {
        
        let params = ["token"    : token]
        AF.request(WRITE_TOKEN, method: .post, parameters: params)
            .validate()
            .responseJSON { response in
                guard response.error == nil else {
                    completion(CONSTANT.CODE_FAIL, (response.error)!.localizedDescription)
                    return
                }

                // check response valud is valid
                guard let value = response.value as? [String: Any] else {
                    completion(CONSTANT.CODE_FAIL, "Internal server error is occured.")
                   return
                }
               
                let isSuccess = value["success"] as? Int
                
                if  isSuccess == 1 {
                    completion(CONSTANT.CODE_SUCCESS, "")
                } else {
                    completion(CONSTANT.CODE_FAIL, "Something went wrong.")
                }
            }
    }
}
